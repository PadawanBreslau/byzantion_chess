# ByzantionChess

[![Gem Version][GV img]][Gem Version]
[![Build Status][BS img]][Build Status]
[![Dependency Status][DS img]][Dependency Status]
[![Code Climate][CC img]][Code Climate]
[![Coverage Status][CS img]][Coverage Status]

## Description

Gem for chess

```ruby
ByzantionChess.new
```

## Anti-Features

TODO: Write a gem anti-features if any

- Does not do this
- Does not do that
- Does not do ...

## Installation

`$ gem install byzantion_chess` or add to your [Gemfile][] this line: `gem 'byzantion_chess'` then run [bundle install][]

## Usage

Just `require 'byzantion_chess'` and then use it as:

### As a fancy tool

TODO

### As a crazy tool

TODO

## Contributing

1. Fork it.
2. Make your feature addition or bug fix and create your feature branch.
3. Update the [Change Log][].
3. Add specs/tests for it. This is important so I don't break it in a future version unintentionally.
4. Commit, create a new Pull Request.
5. Check that your pull request passes the [build][travis pull requests].

## License

Released under the MIT License. See the [LICENSE][] file for further details.

## Links

[RubyGems][] | [Documentation][] | [Source][] | [Bugtracker][] | [Build Status][] | [Dependency Status][] | [Code Climate][]


[bundle install]: http://gembundler.com/man/bundle-install.1.html
[Gemfile]: http://gembundler.com/man/gemfile.5.html
[LICENSE]: LICENSE.md
[Change Log]: CHANGELOG.md

[RubyGems]: https://rubygems.org/gems/byzantion_chess
[Documentation]: http://rubydoc.info/gems/byzantion_chess
[Source]: https://github.com/TODO: Write your github username/byzantion_chess
[Bugtracker]: https://github.com/TODO: Write your github username/byzantion_chess/issues

[travis pull requests]: https://travis-ci.org/TODO: Write your github username/byzantion_chess/pull_requests

[Gem Version]: https://rubygems.org/gems/byzantion_chess
[Build Status]: https://travis-ci.org/TODO: Write your github username/byzantion_chess
[Dependency Status]: https://gemnasium.com/TODO: Write your github username/byzantion_chess
[Code Climate]: https://codeclimate.com/github/TODO: Write your github username/byzantion_chess
[Coverage Status]: https://coveralls.io/r/TODO: Write your github username/byzantion_chess

[GV img]: https://badge.fury.io/rb/byzantion_chess.png
[BS img]: https://travis-ci.org/TODO: Write your github username/byzantion_chess.png
[DS img]: https://gemnasium.com/TODO: Write your github username/byzantion_chess.png
[CC img]: https://codeclimate.com/github/TODO: Write your github username/byzantion_chess.png
[CS img]: https://coveralls.io/repos/TODO: Write your github username/byzantion_chess/badge.png?branch=master
